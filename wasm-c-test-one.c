
// // Using .so
// gcc -Wall -I../wasmtime-dev-x86_64-linux-c-api/include/ -L../wasmtime-dev-x86_64-linux-c-api/lib/ wasm-c-test-one.c -o wasm-c-test-one -lwasmtime && LD_LIBRARY_PATH=../wasmtime-dev-x86_64-linux-c-api/lib/ ./wasm-c-test-one

// // Using .a
// gcc -Wall -I../wasmtime-dev-x86_64-linux-c-api/include/ -L../wasmtime-dev-x86_64-linux-c-api/lib/ wasm-c-test-one.c -o wasm-c-test-one -l:libwasmtime.a -lpthread -ldl -lm && ./wasm-c-test-one

#include <stdlib.h>
#include <stdio.h>

#include "wasm.h"


// via: xxd -i simple.wasm > simple_wasm.h
#include "simple_wasm.h"


wasm_engine_t *the_engine;

wasm_store_t *the_store;


int main(int argc, char **argv) {

  printf("[start]\n");


  printf("[init]\n");

  //
  // See <https://github.com/WebAssembly/wasm-c-api/blame/5c742b048f7766a0c00be3a7af23fb71ba816026/README.md#L47-L49>:
  //
  //   > * The VM must be initialised by creating an instance of an *engine* (`wasm::Engine`/`wasm_engine_t`)
  //   >   and is shut down by deleting it. Such an instance may only be created once per process.
  //   >
  //   > * All runtime objects are tied to a specific *store* (`wasm::Store`/`wasm_store_t`). Multiple stores
  //   >   can be created, but their objects cannot interact. Every store and its objects must only be accessed
  //   >   in a single thread.
  //

  the_engine = wasm_engine_new(); // created once per process.

  printf("  %p\n", the_engine);

  the_store = wasm_store_new(the_engine); // created per desired isolated "environment".

  printf("  %p\n", the_store);


  printf("[read bytecode]\n");

  wasm_byte_vec_t bytecode;
  wasm_byte_vec_new_uninitialized(&bytecode, simple_wasm_len);

  printf("  %ld\n", bytecode.size);
  printf("  %p\n", bytecode.data);

  for (int i=0; i<simple_wasm_len; i++) { // note: bug? still valid when starting from 1?
    bytecode.data[i] = simple_wasm[i];
    printf("  %02x", bytecode.data[i]);
  }

  printf("\n");


  printf("[validate module]\n");

  if (!wasm_module_validate(the_store, &bytecode)) {
    printf("  not valid. exiting.\n");
    return 1;
  } else {
    printf("  valid\n");
  }


  printf("[compile module]\n");

  wasm_module_t *the_module = wasm_module_new(the_store, &bytecode);

  printf("  %p\n", the_module);

  if (!the_module) {
    printf("  compile failed. exiting.\n");
    return 1;
  }

  wasm_byte_vec_delete(&bytecode);


  printf("[instantiate]\n");

  wasm_instance_t *module_instance = wasm_instance_new(the_store, the_module, NULL /*imports*/, NULL /*trap*/);

  printf("  %p\n", module_instance);

  if (!module_instance) {
    printf("  failed to instantiate. exiting.\n");
    return 1;
  }


  printf("[extract exports]\n");

  wasm_extern_vec_t the_exports;

  wasm_instance_exports(module_instance, &the_exports);

  printf("  num found: %ld\n", the_exports.size);

  if (the_exports.size == 0) {
    printf("  no exports found. exiting.\n");
    return 1;
  }

  wasm_externkind_t the_extern_kind = wasm_extern_kind(the_exports.data[0]);

  printf("  kind: %d\n", the_extern_kind);

  if (the_extern_kind != WASM_EXTERN_FUNC) {
    printf("  export not a function. exiting.\n");
    return 1;
  }

  wasm_func_t *the_func = wasm_extern_as_func(the_exports.data[0]);

  printf("  %p\n", the_func);

  if (!the_func) {
    printf("  func get failed. exiting.\n");
    return 1;
  }

  printf("  num params: %ld\n", wasm_func_param_arity(the_func));
  printf("  num results: %ld\n", wasm_func_result_arity(the_func));

  // TODO: Add validity checks...


  printf("[calling]\n");

  wasm_val_t results[1];
  wasm_trap_t *it_is_a_trap = wasm_func_call(the_func, NULL, results);

  if (it_is_a_trap) {
    printf("  func call failed. exiting.\n");
    return 1;
  }

  printf("  result: %d\n", results[0].of.i32);


  printf("[cleanup]\n");

  wasm_extern_vec_delete(&the_exports);

  wasm_instance_delete(module_instance);

  wasm_module_delete(the_module);

  wasm_store_delete(the_store);

  //
  // See <https://github.com/WebAssembly/wasm-c-api/blob/b0b4bfb032af537fa747a505eba222eb1e35edd9/include/wasm.h#L57>:
  //
  //   > Own data is created by `wasm_xxx_new` functions and some others.
  //   > It must be released with the corresponding `wasm_xxx_delete` function.
  //
  // Note: The `wasm_engine_delete()` function was created via the `WASM_DECLARE_OWN` macro.
  //

  wasm_engine_delete(the_engine);


  printf("[end]\n");

  return 0;
}
