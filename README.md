### Wasmtime WASM (WebAssembly) VM non-Web C Embedding Example

Developed as a proof-of-concept while I was figuring out how to get
`libwasmtime` WASM C API working.

See also:

  * <https://github.com/bytecodealliance/wasmtime>
  * <https://github.com/WebAssembly/wasm-c-api/>
  