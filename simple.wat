;;
;; Very simple `.wat` WebAssembly Text example
;;
;; Derived from examples at <https://github.com/WebAssembly/wasm-c-api/tree/b0b4bfb032af537fa747a505eba222eb1e35edd9/example>
;;
;; Can be compiled to byte code & then interpreted with
;; tools from <https://github.com/WebAssembly/wabt>:
;;
;;    $ wat2wasm -v simple.wat -o simple.wasm
;;    [...snip byte code dump...]
;;
;;    $ wasm-interp --run-all-exports simple.wasm
;;    f() => i32:4
;;

(module
  (func (export "f") (result i32) (i32.const 4))
)
